import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import * as L from 'leaflet';
import 'leaflet.vectorgrid';
import 'leaflet/dist/leaflet.css';

@Injectable()
export class MapService {
    public map: L.Map;
    public baseMaps: any;
    private vtLayer: any;

    constructor(private http: Http) {
        this.baseMaps = {
            OpenStreetMap: L.tileLayer('http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
                attribution: ' OpenStreetMap Team</a>'
            })
        };
    }

    disableMouseEvent(elementId: string) {
        const element = <HTMLElement>document.getElementById(elementId);

        L.DomEvent.disableClickPropagation(element);
        L.DomEvent.disableScrollPropagation(element);
    }

}
