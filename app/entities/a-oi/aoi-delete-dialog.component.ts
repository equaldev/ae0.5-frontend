import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AOI } from './aoi.model';
import { AOIPopupService } from './aoi-popup.service';
import { AOIService } from './aoi.service';

@Component({
    selector: 'jhi-aoi-delete-dialog',
    templateUrl: './aoi-delete-dialog.component.html'
})
export class AOIDeleteDialogComponent {

    aOI: AOI;

    constructor(
        private aOIService: AOIService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.aOIService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'aOIListModification',
                content: 'Deleted an aOI'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-aoi-delete-popup',
    template: ''
})
export class AOIDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private aOIPopupService: AOIPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.aOIPopupService
                .open(AOIDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
