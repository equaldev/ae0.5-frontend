import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AOIComponent } from './aoi.component';
import { AOIDetailComponent } from './aoi-detail.component';
import { AOIPopupComponent } from './aoi-dialog.component';
import { AOIDeletePopupComponent } from './aoi-delete-dialog.component';

@Injectable()
export class AOIResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const aOIRoute: Routes = [
    {
        path: 'aoi',
        component: AOIComponent,
        resolve: {
            'pagingParams': AOIResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'afterEarthApp.aOI.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'aoi/:id',
        component: AOIDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'afterEarthApp.aOI.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const aOIPopupRoute: Routes = [
    {
        path: 'aoi-new',
        component: AOIPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'afterEarthApp.aOI.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'aoi/:id/edit',
        component: AOIPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'afterEarthApp.aOI.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'aoi/:id/delete',
        component: AOIDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'afterEarthApp.aOI.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
