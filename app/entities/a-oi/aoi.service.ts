import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { AOI } from './aoi.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class AOIService {

    private resourceUrl = 'api/a-ois';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(aOI: AOI): Observable<AOI> {
        const copy = this.convert(aOI);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(aOI: AOI): Observable<AOI> {
        const copy = this.convert(aOI);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<AOI> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.before = this.dateUtils
            .convertLocalDateFromServer(entity.before);
        entity.after = this.dateUtils
            .convertLocalDateFromServer(entity.after);
    }

    private convert(aOI: AOI): AOI {
        const copy: AOI = Object.assign({}, aOI);
        copy.before = this.dateUtils
            .convertLocalDateToServer(aOI.before);
        copy.after = this.dateUtils
            .convertLocalDateToServer(aOI.after);
        return copy;
    }
}
