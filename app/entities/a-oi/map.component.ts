import { Component, ElementRef, ViewEncapsulation, Input, NgModule, ViewChild, AfterViewInit, OnInit, OnDestroy } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { AOI } from './aoi.model';

declare const L: any; // --> Works
import 'leaflet.vectorgrid';
import 'leaflet-draw';
import 'leaflet-measure-path';
import 'leaflet.fullscreen';
import 'leaflet-easyprint';
import 'leaflet.browser.print/dist/leaflet.browser.print.min.js';
//import 'leaflet-measure';
import 'leaflet-draw/dist/leaflet.draw.css';
import 'leaflet/dist/leaflet.css';

@Component({
  selector: 'ae-map',
  template: `<div id="map" #map ></div>
  <br>
    <a href='#' id='export'>Export Features</a>
    <a href='#' id='import'>Import Features</a>
    <button class="btn btn-success" (click)="manualPrint()" id='download'>Download Map</button>
  `,
  styleUrls: ['./map.service.css',
    './leaflet.draw.css',
    './leaflet-measure-path.css',
    './fs/Control.FullScreen.css',

  ],
  encapsulation: ViewEncapsulation.None,

})
export class MiniMap implements OnInit, OnDestroy, AfterViewInit {
  map: L.Map = null;

  @ViewChild('map') mapContainer;
  @Input() aoi: AOI;
  before: Date;
  after: Date;
  printer = L.easyPrint({
    sizeModes: ['Current', 'A4Landscape', 'A4Portrait'],
    filename: 'ae-export',
    exportOnly: true,
  })

  constructor(private elementRef: ElementRef) {
  }

  ngOnInit() {


    // Preparing the AOI
    this.after = this.aoi.after.toISOString();
    this.before = this.aoi.before.toISOString();


    //   console.log(this.aoi);
    // let bounds = new L.LatLngBounds(new L.LatLng(this.aoi.lat, this.aoi.lng), new L.LatLng(61.2, 2.5));

    //Init Map
    this.map =
      L.map(this.mapContainer.nativeElement)
        .setView([this.aoi.lng, this.aoi.lat], 13);

    //Initiate boundary to reduce map tile requests
    let mBounds = this.map.getBounds();
    this.map.setMaxBounds(mBounds);

    // Tools

    this.printer.addTo(this.map);

    let drawnItems = new L.FeatureGroup();
    this.map.addLayer(drawnItems);

    let toPDF = L.browserPrint({
      title: 'Export as PDF',
      closePopupsOnPrint: false,
      printModesNames: { 
        Portrait: "Portrait", 
        Landscape: "Landscape", 
        Custom: "Manually select area" }
    }).addTo(this.map);


// this.map.on("browser-pre-print", function(e:any){
// 		/*on print start we already have a print map and we can create new control and add it to the print map to be able to print custom information */
//                        console.log(e);
//     drawnItems.addTo(e.printMap);
//     //L.FeatureGroup().addLayer(drawnItems).addTo(e.printMap);

//   });
  

    L.control.scale().addTo(this.map);

    L.control.fullscreen({
      position: 'topleft', // change the position of the button can be topleft, topright, bottomright or bottomleft, defaut topleft 
      title: 'Open Fullscreen', // change the title of the button, default Full Screen 
      titleCancel: 'Exit Fullscreen', // change the title of the button when fullscreen is on, default Exit Full Screen 
      content: null, // change the content of the button, can be HTML, default null 
      forceSeparateButton: true, // force seperate button to detach from zoom buttons, default false 
      forcePseudoFullscreen: false, // force use of pseudo full screen even if full screen API is available, default false 
      fullscreenElement: false // Dom element to render in full screen, false by default, fallback to map._container 
    }).addTo(this.map);



    // Shape Options for Features.
    // -------------------------------------
    // Will eventually implement the user customising the controls (color, measurement etc)
    // Will involve setting these options from storage

    let drawControl = new L.Control.Draw({
      edit: {
        featureGroup: drawnItems
      },
      draw: {
        polyline: {
          shapeOptions: {
            color: 'red',
            showLength: true,
            showMeasurements: true
          },
          metric: true,
          showLength: true,
          showMeasurements: true
        },
        rectangle: {
          shapeOptions: {
            color: 'green',
            showArea: true,
            showMeasurements: true
          },
          metric: true,
          showArea: true,
          showMeasurements: true
        },
        polygon: {
          shapeOptions: {
            color: 'purple',
            showArea: true,
            showMeasurements: true
          },
          showArea: true,
          showMeasurements: true
        }
      }
    });

    this.map.addControl(drawControl);


    // Handlers for creating measurements and features.
    this.map.on('draw:created', function (e: any) {
      let type = e.layerType,
        layer = e.layer;

      if (type === 'marker') {

        let name = prompt("enter a title");
        let desc = prompt("enter a description");
        let currentTime = new Date();

        console.log(layer);
        layer.bindPopup('<span><b>Name:   </b></span>' + name +
          '<br/><br/><span><b>Description:   </b></span>' + desc +
          '<br/><br/><span><b>Latitude:   </b></span>' + layer._latlng.lat +
          '<br/><br/><span><b>Longitude:   </b></span>' + layer._latlng.lng +
          '<br/><br/><span><b>Created:   </b></span>' + currentTime +
          '<br/>');

      }

      if (type === 'polyline') {

        let name = prompt("enter a title");
        let desc = prompt("enter a description");
        let currentTime = new Date();

        console.log(layer);
        layer.bindPopup('<span><b>Name:   </b></span>' + name +
          '<br/><br/><span><b>Description:   </b></span>' + desc +
          // Find way to add total length of line to popup
          // '<br/><br/><span><b>Latitude:   </b></span>'+ layer._measurementLayer._layers[lengthIndex]._measurement +
          '<br/><br/><span><b>Created:   </b></span>' + currentTime +
          '<br/>');
        // let _length = L.GeometryUtil.length([layer._latlngs["0"], layer._latlngs[layer._latlngs.length - 1]]);
        // console.log(_length);

      }

      if (type === 'polygon') {

        let name = prompt("enter a title");
        let desc = prompt("enter a description");
        let currentTime = new Date();

        let seeArea = L.GeometryUtil.geodesicArea(layer.getLatLngs()[0]);
        //let seeaArea = L.GeometryUtil.geodesicArea(layer._latlngs[0]);

        layer.bindPopup('<span><b>Name:   </b></span>' + name +
          '<br/><br/><span><b>Description:   </b></span>' + desc +
          '<br/><br/><span><b>Area:   </b></span>' + (seeArea / 100000).toFixed(2) + " km<sup>2</sup>" +
          '<br/><br/><span><b>Created:   </b></span>' + currentTime +
          '<br/>');

      }


      drawnItems.addLayer(layer);
    });

    document.getElementById('export').onclick = function (e) {
      // Extract GeoJson from featureGroup
      let data = drawnItems.toGeoJSON();

      // Stringify the GeoJson
      let convertedData = 'text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(data));

      // Create export
      document.getElementById('export').setAttribute('href', 'data:' + convertedData);
      document.getElementById('export').setAttribute('download', 'data.geojson');
    }


    // Confirm we've got 'em by displaying them to the screen
    const apiKey = 'BB70FD54EBB7483882BC',
      apiSecret = 'A69E7AE6C49C455C856386A130DF299D';


    const rgbLayerBefore = `https://tile-{s}.urthecast.com/v1/rgb/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=10&acquired_lte=${this.before}`,// + before,
      ndviLayerBefore = `https://tile-{s}.urthecast.com/v1/ndvi/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=10&acquired_lte=${this.before}`,
      ndwiLayerBefore = `https://tile-{s}.urthecast.com/v1/ndwi/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=10&acquired_lte=${this.before}`,
      fcnirLayerBefore = `https://tile-{s}.urthecast.com/v1/false-color-nir/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=10&acquired_lte=${this.before}`,
      eviLayerBefore = `https://tile-{s}.urthecast.com/v1/evi/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=10&acquired_lte=${this.before}`;

    const rgbLayerAfter = `https://tile-{s}.urthecast.com/v1/rgb/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=10&acquired_lte=${this.after}`, //+ after,
      ndviLayerAfter = `https://tile-{s}.urthecast.com/v1/ndvi/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=10&acquired_lte=${this.after}`,
      ndwiLayerAfter = `https://tile-{s}.urthecast.com/v1/ndwi/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=10&acquired_lte=${this.after}`,
      fcnirLayerAfter = `https://tile-{s}.urthecast.com/v1/false-color-nir/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=10&acquired_lte=${this.after}`,
      eviLayerAfter = `https://tile-{s}.urthecast.com/v1/evi/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}&cloud_coverage_lte=10&acquired_lte=${this.after}`;

    const urlLayer3 = `https://api.mapbox.com/styles/v1/mapbox/satellite-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZXF1aWxsaWJyaXVtIiwiYSI6IjRiM2YwNGI3OTgxODRjNjhhZWJlMTVmNDQ5YzIwZWI1In0.la27BQntMyeBoN-Ea4m45w`;
    const urlLayer4 = `http://{s}.tile.osm.org/{z}/{x}/{y}.png`;
    const urlLayer5 = `http://{s}.tile.opentopomap.org/{z}/{x}/{y}.png'`;

    // These tiles be for testing.

    this.addLayer(L.tileLayer(urlLayer3,{attribution: '© MapBox. Mapbox.com'}).addTo(this.map), 'HiRes', 1);
    this.addLayer(L.tileLayer(urlLayer4,{attribution: '© OpenStreetMap contributors. OpenStreetMap.org'}).addTo(this.map), 'OSM', 1);
    this.addLayer(L.tileLayer(urlLayer5,{attribution: '© OpenStreetMap contributors. OpenTopoMap.org'}).addTo(this.map), 'Topographic', 1);


    // These tiles are the money makers.

    // this.addLayer(L.tileLayer(rgbLayerBefore).addTo(this.map), 'RGB - Before', 1);
    // this.addLayer(L.tileLayer(ndviLayerBefore).addTo(this.map), 'NDVI - Before', 1);
    // this.addLayer(L.tileLayer(ndwiLayerBefore).addTo(this.map), 'NDWI - Before', 1);
    // this.addLayer(L.tileLayer(fcnirLayerBefore).addTo(this.map), 'False Color NIR - Before', 1);
    // this.addLayer(L.tileLayer(eviLayerBefore).addTo(this.map), 'EVI - Before', 1);
    // this.addLayer(L.tileLayer(rgbLayerAfter).addTo(this.map), 'RGB - After', 1);
    // this.addLayer(L.tileLayer(ndviLayerAfter).addTo(this.map), 'NDVI - After', 1);
    // this.addLayer(L.tileLayer(ndwiLayerAfter).addTo(this.map), 'NDWI - After', 1);
    // this.addLayer(L.tileLayer(fcnirLayerAfter).addTo(this.map), 'False Color NIR - After', 1);
    // this.addLayer(L.tileLayer(eviLayerAfter).addTo(this.map), 'EVI - After', 1);

  }

  addLayer(layer: any, name: any, zIndex) {
    layer
      .setZIndex(zIndex)
      .addTo(this.map);
    const aMap = this.map;
    const link = document.createElement('a');
    link.href = '#';
    link.className = 'active';
    link.innerHTML = name;

    link.onclick = function (e: any) {
      e.preventDefault();
      e.stopPropagation();

      if (aMap.hasLayer(layer)) {
        aMap.removeLayer(layer);
        this.className = '';
      } else {
        aMap.addLayer(layer);
        this.className = 'active';
      }
    };

    const layers = document.getElementById('map-ui');

    layers.appendChild(link);
  }

  public saveFeature(event: any, layer: any) {
    // let sName =  (<HTMLInputElement>document.getElementById("shapeName")).value;
    //let sDesc =  (<HTMLInputElement>document.getElementById("shapeDesc")).value;
    console.log(event, layer);

    //  var drawings = drawnItems.getLayers();  //drawnItems is a container for the drawn objects
    //  drawings[drawings.length - 1].title = sName;
    //  drawings[drawings.length - 1].content = sDesc;
    //this.map.closePopup();
  }
  manualPrint() {

     this.printer.printMap('CurrentSize', 'ae-export');

  }

  ngAfterViewInit() {
    //Fixes bug with blank map tiles
    this.map.invalidateSize();

    // this.elementRef.nativeElement.querySelector(".saveF")
    // .addEventListener('click', (e)=>
    // {
    //   // get id from attribute
    //   //var merchId = e.target.getAttribute("data-merchId");
    //   console.log(e);
    //  // this.saveFeature();
    // });
  }

  ngOnDestroy() { }
}
