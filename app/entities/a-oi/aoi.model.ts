import { BaseEntity } from './../../shared';

export class AOI implements BaseEntity {
    constructor(
        public id?: number,
        public lat?: string,
        public lng?: string,
        public before?: any,
        public after?: any,
        public title?: string,
        public description?: string,
    ) {
    }
}
