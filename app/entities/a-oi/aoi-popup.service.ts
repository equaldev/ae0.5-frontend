import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AOI } from './aoi.model';
import { AOIService } from './aoi.service';

@Injectable()
export class AOIPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private aOIService: AOIService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.aOIService.find(id).subscribe((aOI) => {
                    if (aOI.before) {
                        aOI.before = {
                            year: aOI.before.getFullYear(),
                            month: aOI.before.getMonth() + 1,
                            day: aOI.before.getDate()
                        };
                    }
                    if (aOI.after) {
                        aOI.after = {
                            year: aOI.after.getFullYear(),
                            month: aOI.after.getMonth() + 1,
                            day: aOI.after.getDate()
                        };
                    }
                    this.ngbModalRef = this.aOIModalRef(component, aOI);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.aOIModalRef(component, new AOI());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    aOIModalRef(component: Component, aOI: AOI): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.aOI = aOI;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
