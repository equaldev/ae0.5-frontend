import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AfterEarthSharedModule } from '../../shared';
import {
    AOIService,
    AOIPopupService,
    AOIComponent,
    AOIDetailComponent,
    AOIDialogComponent,
    AOIPopupComponent,
    AOIDeletePopupComponent,
    AOIDeleteDialogComponent,
    aOIRoute,
    aOIPopupRoute,
    AOIResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...aOIRoute,
    ...aOIPopupRoute,
];

@NgModule({
    imports: [
        AfterEarthSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AOIComponent,
        AOIDetailComponent,
        AOIDialogComponent,
        AOIDeleteDialogComponent,
        AOIPopupComponent,
        AOIDeletePopupComponent,
    ],
    entryComponents: [
        AOIComponent,
        AOIDialogComponent,
        AOIPopupComponent,
        AOIDeleteDialogComponent,
        AOIDeletePopupComponent,
    ],
    providers: [
        AOIService,
        AOIPopupService,
        AOIResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AfterEarthAOIModule {}
