import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AOI } from './aoi.model';
import { AOIPopupService } from './aoi-popup.service';
import { AOIService } from './aoi.service';

@Component({
    selector: 'jhi-aoi-dialog',
    templateUrl: './aoi-dialog.component.html'
})
export class AOIDialogComponent implements OnInit {

    aOI: AOI;
    isSaving: boolean;
    beforeDp: any;
    afterDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private aOIService: AOIService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.aOI.id !== undefined) {
            this.subscribeToSaveResponse(
                this.aOIService.update(this.aOI));
        } else {
            this.subscribeToSaveResponse(
                this.aOIService.create(this.aOI));
        }
    }

    private subscribeToSaveResponse(result: Observable<AOI>) {
        result.subscribe((res: AOI) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: AOI) {
        this.eventManager.broadcast({ name: 'aOIListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-aoi-popup',
    template: ''
})
export class AOIPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private aOIPopupService: AOIPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.aOIPopupService
                    .open(AOIDialogComponent as Component, params['id']);
            } else {
                this.aOIPopupService
                    .open(AOIDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
